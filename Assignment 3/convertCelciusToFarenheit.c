#include <stdio.h>
/*
Conversion calculator
  1. Celcius to Farenheit
  2. Farenheit to Celcius
*/

int main() {

    float celcius;
    float farenheit;
    int choice = 0;

    printf("Temperature Conversion Calculator:\n");
    printf("=================================\n");
    printf("1. Convert to C.\n");
    printf("2. Convert to F.\n");
    printf("3. To exit the program.\n");
    printf("=================================\n");

    while(choice != 3) {
        printf("Enter your choice: ");
        scanf("%d", &choice);
        switch (choice) {
          case 1: 
            printf("Enter the temperature in Farenheit: ");
            scanf("%f", &farenheit);
            printf("The result is %f C\n", 5/9.0 * (farenheit -32));
            break;
          case 2:
            printf("Enter the temperature in Celcius: ");
            scanf("%f", &celcius);
            printf("The result is %f F\n", 9/5.0 * celcius + 32);
            break;
        }
        printf("=================================\n");
    }

    return 0;
}