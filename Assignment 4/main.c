#include <stdio.h>
#include <string.h>

int main(void) {
  
  char str1[100];
  char str2[100];

  printf("Comparison of two strings.\n");
  printf("Enter first string: ");
  scanf("%s", str1);
  printf("Enter second string: ");
  scanf("%s", str2);

  if (strcmp(str1, str2) == 0) {
    printf("Two strings are equal\n");
  } else {
    printf("Two strings are not equal\n");
  }
  return 0;
}