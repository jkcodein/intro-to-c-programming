
void autoExamples();
void breakExamples();
void caseExamples();
void charExamples();
void constExamples();
void continueExamples();
void defaultExamples();
void doExamples();
void doubleExamples();
void elseExamples();
void enumExamples();
void externExamples();
void floatExamples();
void forExamples();
void gotoExamples();
void ifExamples();
void intExamples();
void longExamples();
void registerExamples();
void returnExamples();
void shortExamples();
void signedExamples();
void sizeofExamples();
void staticExamples();
void structExamples();
void switchExamples();
void typedefExamples();
void unionExamples();
void unsignedExamples();
void voidExamples();
void volatileExamples();
void whileExamples();
void _PackedExamples();

int main() {

    return 0;
}