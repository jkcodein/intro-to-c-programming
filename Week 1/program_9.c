/*
  Calculate an area of a rectangle.

  Requirements:  
    Read input from stdin
    Write output from stdout

  Functions:
    printf() // write output to stdout
    scanf() // read input from stdin and format to variable
*/

int main() {
   int height = 0;
   int width = 0;
   int area = 0;

   printf("Enter height in cm: ");
   scanf("%d", &height);
   printf("Enter width in cm: ");
   scanf("%d", &width);

   area = height * width;
   printf("Area of a rectangle is: %d cm2\n", area);
   return 0;
}

