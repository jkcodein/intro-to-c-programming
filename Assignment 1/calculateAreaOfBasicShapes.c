#include <stdio.h>
#include <stdlib.h>

/*
1. Area of Circle: PI * radius * radius
2. Area of Square: length * length
3. Area of Triangle: 0.5 * base * height
*/

int main() {

    float PI = 3.1415;
    float radius = 0;
    float length = 0;
    float base = 0;
    float height = 0;
    int quit = 2;
    int choice;

    while(quit == 2) {
        
        printf("Choose one of the following options:\n");

        printf("1. Area of Circle: PI * radius * radius\n");
        printf("2. Area of Square: length * length\n");
        printf("3. Area of Triangle: (1/2) * base * height\n");
        printf("Enter an index number: ");
        scanf("%d", &choice);
        switch (choice) {
            case 1:
                printf("Enter a radius: ");
                scanf("%f", &radius);
                printf("Area of Circle is %f\n", PI * radius * radius);
                break;
            case 2:
                printf("Enter a length: ");
                scanf("%f", &length);
                printf("Area of Square is %f\n", length * length);
                break;
            case 3:
                printf("Enter base and height: ");
                scanf("%f %f", &base, &height);
                printf("Area of Triangle is %f\n", 0.5 * base * height);
                break;
        }
        printf("Would you like to quit? [1. Yes, 2. No]: ");
        scanf("%d", &quit);
    }

    return 0;
}