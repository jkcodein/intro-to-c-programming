#include <stdio.h>

/*
  Calculation of average value of numbers
  * input number of data
  * input numbers 
  * store numbers in array
  * sum all numbers of array
  * calculate average value (sum / number of data)
*/

float average(float a[], int n);

int main() {
  printf("Student Name: Jamshedshoh Kamolzoda\n");
  printf("Student ID: 101079088\n");
  
  float a[100];

  int numberOfData;
  printf("Enter number of data: ");
  scanf("%d", &numberOfData);
  while(numberOfData < 1 || numberOfData > 100) {
    printf("Enter number of data between 1 and 100: ");
    scanf("%d", &numberOfData);
  }
  
  int i = 0;
  float number;
  float sum = 0; 
  while(i < numberOfData) {
    printf("Enter number %d: ", i+1);
    scanf("%f", &a[i]);
    i++;
  }

  printf("Average value of numbers is %f\n", average(a,numberOfData));
  return 0;
}

float average(float a[], int n) {
  float sum = 0;
  for (int i = 0; i < n; i++) {
    sum += a[i]; 
  }
  return sum / n;
}