#include <stdio.h>
#include <math.h>

/*
  Calculation of standard deviation of numbers
  * input number of data
  * input numbers 
  * store numbers in array
  * sum all numbers of array
  * calculate average value (sum / number of data)
  * calculate deviation value using standard deviation formula (sqrt(sum / (n-1)))
*/

float standard_deviation(float data[], int n);

int main() {
  printf("Student Name: Jamshedshoh Kamolzoda\n");
  printf("Student ID: 101079088\n");
  
  float a[100];

  int numberOfData;
  printf("Enter number of data: ");
  scanf("%d", &numberOfData);
  while(numberOfData < 1 || numberOfData > 100) {
    printf("Enter number of data between 1 and 100: ");
    scanf("%d", &numberOfData);
  }
  
  int i = 0;
  float number;
  float sum = 0; 
  while(i < numberOfData) {
    printf("Enter number %d: ", i+1);
    scanf("%f", &a[i]);
    i++;
  }  

  printf("Deviation value of numbers is %f\n", standard_deviation(a, numberOfData));
  return 0;
}

float standard_deviation(float a[], int n) {

  float sum = 0;

  for (int i = 0; i < n; i++) {
    sum += a[i]; 
  }

  float average = sum / n;
  
  sum = 0;
  for (int i = 0; i < n; i++) {
    sum += (a[i] - average) * (a[i] - average);
  }

  return sqrt(sum / (n - 1));
}