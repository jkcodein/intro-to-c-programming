#include <stdio.h>

/*
  Perform mathematical operation of two integers
    * Addition
    * Subtraction
    * Multiplication
    * Division
*/

int main(void) {
  printf("Student name: Jamshedshoh Kamolzoda\n");
  printf("Student ID: 101079088\n");

  int first_number, second_number;
  int choice;
  printf("Enter first integer: ");
  scanf("%d", &first_number);
  printf("Enter second integer: ");
  scanf("%d", &second_number);

  printf("Operation list: \n");
  printf("1. Add numbers\n");
  printf("2. Subtract numbers\n");
  printf("3. Multiple numbers\n");
  printf("4. Divide numbers\n");
  printf("Enter an index of operation: ");
  scanf("%d", choice);

  switch(choice) {
    case 1: 
      printf("Addition of numbers is %d\n", first_number + second_number);
      break;
    case 2: 
      printf("Subtraction of numbers is %d\n", first_number - second_number);
      break;
    case 3: 
      printf("Multiplication of numbers is %d\n", first_number * second_number);
      break;
    case 4:
      if (second_number == 0) {
        printf("Division by 0 (zero) is undefined\n");
      } else {
        printf("Division of numbers is %d\n", first_number / second_number);
      }
      break;
  }
  return 0;
}