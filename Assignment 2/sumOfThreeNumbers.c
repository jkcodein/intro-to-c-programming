#include <stdio.h>
#include <stdlib.h>

/*
Sum three numbers
*/

int main() {
    float a, b, c;
    printf("Enter three numbers to sum them: ");
    scanf("%f %f %f", &a, &b, &c);
    printf("The sum is %f", a + b + c);
    return 0;
}