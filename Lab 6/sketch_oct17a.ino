/*
 * Student Name: Jamshedshoh Kamolzoda
 * Student ID: 101079088
 * Day: Wednesday 12 Noon
 * Date: 17/10/2018
 */
/*
 * Lab 6: Exercise 1: Blink LED Curcuit
 */


int ledPin = 13;

void setup() {
  pinMode(ledPin, OUTPUT);
}

void loop() {
  digitalWrite(ledPin, HIGH);
  delay(1000);
  digitalWrite(ledPin, LOW);
  delay(1000);
}
