/*
 * Student Name: Jamshedshoh Kamolzoda
 * Student ID: 101079088
 * Day: Wednesday 12 Noon
 * Date: 17/10/2018
 */

 /*
  * Lab 6: Exercise 2: Digital Read Serial 
  */

int inputPin = 2;

void setup() {
  Serial.begin(9600);
  pinMode(inputPin, INPUT);
}

void loop() {
  int sensorValue = digitalRead(inputPin);
  Serial.println(sensorValue);
}
